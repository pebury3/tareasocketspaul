package TCP;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class ClienteTCP {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Scanner scn = new Scanner(System.in);
        String mensaje=null, respuescli;
        System.out.println("Creando y conectando el socket stream cliente");

        try {
            Socket cliente = new Socket("localhost", 55000);

            //Stream de entrada de datos
            InputStream is = cliente.getInputStream();
            DataInputStream dis = new DataInputStream(is);
            //Stream de salida de datos
            OutputStream os = cliente.getOutputStream();
            DataOutputStream dos = new DataOutputStream(os);

            // mensajes del servidor inforando que es
            mensaje = dis.readUTF();
            System.out.println(mensaje);
            // mensajes del servidor pidiendo un numero
            mensaje = dis.readUTF();
            System.out.println(mensaje);
            //enviando el primer numero 1
            String num1 = scn.nextLine();
            dos.writeUTF(num1);
            System.out.println("Enviando mensaje al servidor:");
            System.out.println("Mensaje enviado");
            System.out.println("Recibiendo mensaje del servidor:");
            //mensaje del servidor pidiendo numero 2
            mensaje = dis.readUTF();
            System.out.println(mensaje);
            //enviando el primer numero 2
            String num2 = scn.nextLine();
            dos.writeUTF(num2);
            System.out.println("Mensaje enviado");

            //servidor pidiendo que operacion hacer
            System.out.println("Recibiendo mensaje del servidor:");

            mensaje = dis.readUTF();
            System.out.println(mensaje);
            mensaje = dis.readUTF();
            System.out.println(mensaje);
            //escrbiendo que operacion hacer
            respuescli = sc.nextLine();
            dos.writeUTF(respuescli);
            System.out.println("operacion enviada");
            mensaje = dis.readUTF();
            System.out.println(mensaje);

                //cerrando coneccion



        } catch (IOException e) {
            System.err.println("Error al crear el socket.");
            System.out.println(e.getMessage());
    }
    }
}

/*
System.out.println("Cerrando conexion con el cliente...");
        dos.close();
        dis.close();
        cliente.close();
        System.out.println("Terminando");
 */

