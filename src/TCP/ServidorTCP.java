package TCP;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ServidorTCP {
    static Lock mutex = new ReentrantLock();
    static int contador = 0;
    static Semaphore semaforo = new Semaphore(5);


    public static void main(String[] args) {


        try {
            //construir el objeto para que escuche por el puerto indicado
            ServerSocket servidor = new ServerSocket(55000);


            System.out.println("Aceptando conexiones...");
            //Espera bloqueado conexiones de clientes
            //Cuando llegue una devolverÃ¡ el accept un Socket
            Socket cliente = servidor.accept();

            Thread[] clientes = new Thread[5];

            for (int i = 0; i < clientes.length; i++) {
                clientes[i] = new Thread((new Cliente(cliente)), " Cliente  " + (i + 1));
                clientes[i].start();
            }

            // Esperamos a que todos los hilos terminen
            for (Thread clicli : clientes) {

                try {

                    clicli.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }


            System.out.println("Conexion establecida!");

            //inicializaciÃ³n de los streams para entreda y salida de datos

            System.out.println("Cerrando Streams y sockets...");

            cliente.close();
            servidor.close();

        } catch (IOException e) {
            System.err.println("Error al crear el socket");
            System.out.println(e.getMessage());
        }

    }

    static class Cliente implements Runnable {



        private Socket cliente;

        public Cliente(Socket cliente) {
            this.cliente = cliente;
        }
        @Override

        public void run() {

            mutex.lock();
            DataInputStream entrada = null;
            DataOutputStream salida = null;
            try {


                entrada = new DataInputStream(cliente.getInputStream());
                salida = new DataOutputStream(cliente.getOutputStream());

                try {
                    // Esperamos adquirir un permiso del semáforo
                    semaforo.acquire();
                    System.out.println(Thread.currentThread().getName() + " entró a la sección crítica.");
                    // Simulamos una operación crítica
                    for (int i = 0; i < 1000; i++) {
                       contador++;
                    }

                    System.out.println("informando al cliente " + Thread.currentThread().getName());
                    salida.writeUTF("Esto es una Calculadora " + Thread.currentThread().getName());
                    salida.writeUTF("dame un numero " + Thread.currentThread().getName());
                    String num1 = entrada.readUTF();
                    do {

                        if (num1.equalsIgnoreCase("salir")) {
                            System.out.println("Hasta luego ! :D");
                            salida.writeUTF("hasta luego ");
                        } else {

                            System.out.println("Esperando numero 1 " + Thread.currentThread().getName());
                            System.out.println(num1);

                            System.out.println("Enviando mensaje al cliente: " + Thread.currentThread().getName());
                            salida.writeUTF("dame el numero dos " + Thread.currentThread().getName());


                            System.out.println("Esperando numero 2 " + Thread.currentThread().getName());
                            String num2 = entrada.readUTF();
                            System.out.println(num2);


                            System.out.println("Enviando mensaje al cliente: " + Thread.currentThread().getName());

                            salida.writeUTF("dime que  operacion hacer " + Thread.currentThread().getName());

                            salida.writeUTF("sumar, restar, multiplicar, dividir " + Thread.currentThread().getName());
                            String respuesta = entrada.readUTF();
                            System.out.println(respuesta);

                            switch (respuesta) {
                                case "sumar":
                                    salida.writeUTF("el resultado es " + (Double.parseDouble(num1) + Double.parseDouble(num2)));
                                    System.out.println("la suma es: " + (Double.parseDouble(num1) + Double.parseDouble(num2)));
                                    break;
                                case "restar":
                                    System.out.println("la resta es: " + (Double.parseDouble(num1) - Double.parseDouble(num2)));
                                    salida.writeUTF("el resultado es " + (Double.parseDouble(num1) - Double.parseDouble(num2)));
                                    break;
                                case "multiplicar":
                                    System.out.println("la multiplicacion es: " + (Double.parseDouble(num1) * Double.parseDouble(num2)));
                                    salida.writeUTF("el resultado es " + (Double.parseDouble(num1) * Double.parseDouble(num2)));
                                    break;
                                case "dividir":
                                    if (Double.parseDouble(num2) != 0) {

                                        salida.writeUTF("el resultado es " + (Double.parseDouble(num1) * Double.parseDouble(num2)));
                                        System.out.println("la division es: " + (Double.parseDouble(num1) - Double.parseDouble(num2)));

                                    } else
                                        System.out.println("no se puede dividir entre 0 ");
                                    break;
                                default:
                                    System.out.println("Esa operaciionn no es valida ");
                                    break;
                            }

                            System.out.println(Thread.currentThread().getName() + " salió de la sección crítica.");
                            // Liberamos el permiso del semáforo

                            salida.writeUTF("el resultado es " + respuesta);
                            System.out.println("aqui hace ALGO ");
                        }

                    } while (num1.equals("salir"));
                    entrada.close();
                    salida.close();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

}
