package UDP;

import java.io.IOException;
import java.net.*;

public class ServidorUDP {
    public static void main(String[] args) {

        try {
            InetSocketAddress addr = new InetSocketAddress("127.0.0.1", 5555);
            DatagramSocket socket = new DatagramSocket(addr);
            System.out.println("Servidor conectado");

            //RecepciÃ³n de mensajes
            DatagramPacket paquete;
            while(true) {
                paquete = new DatagramPacket(new byte[1024], 1024);
                //Espera bloqueante mientras recibo la informaciÃ³n
                socket.receive(paquete);

                //Al recibir un paquete, recupero el mensaje, la ip, y el puerto del cliente
                String mensaje = new String(paquete.getData());
                //mensaje = mensaje.replaceAll("[^\\p{Print}]", "");
                System.out.println("Recibiendo mensaje del cliente: " + mensaje);

                InetAddress cliente = paquete.getAddress();
                int puerto_cliente = paquete.getPort();

                //EnvÃ­o de mensaje al cliente:
                byte[] respuesta = "Mensaje desde el servidor".getBytes();

                paquete = new DatagramPacket(respuesta, respuesta.length, cliente, puerto_cliente);
                socket.send(paquete);
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }
}
