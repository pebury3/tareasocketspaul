package UDP;

import java.io.IOException;
import java.net.*;

public class ClienteUDP {
    public static void main(String[] args) {

        try {
            String mensaje = "Mensaje desde el cliente";
            DatagramSocket datagramSocket = new DatagramSocket();
            InetAddress serverAddr = InetAddress.getByName("localhost");

            //construimos el paquete
            DatagramPacket paquete = new DatagramPacket(mensaje.getBytes(), mensaje.length(), serverAddr, 5555);
            datagramSocket.send(paquete);
            System.out.println("menesaje enviado");

            //Esperamos respuesta del server
            byte[] respuesta = new byte[1024];
            paquete = new DatagramPacket(respuesta, respuesta.length);
            datagramSocket.receive(paquete);
            System.out.println("Mensaje recibido: " + new String(respuesta));

            datagramSocket.close();

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }
}
